from flask import Flask,request, make_response, render_template, redirect, url_for
from werkzeug.utils import secure_filename  # 使用这个是为了确保filename是安全的 from os import path
# from os import path
import os
from flask import Flask
from flask import render_template
from flask_bootstrap import Bootstrap
from configure import APP_STATIC_TXT
from WORD.wordCut import main
from WORD import mysql
from WORD.sql import initSQL
import datetime

app = Flask(__name__)
bootstrap = Bootstrap(app)


@app.route("/")
def hello():
	return render_template('home.html')


@app.route("/upload", methods=['GET', 'POST'])
def upload():
	if request.method == 'POST':
		f = request.files["file"]
		base_path = os.path.abspath(os.path.dirname(__file__))
		upload_path = os.path.join(base_path, 'static\\')
		file_name = upload_path + secure_filename(f.filename)
		f.save(file_name)
		initSQL()
		msql = mysql.MySQL()
		sql_ = "INSERT INTO `fenci_result`(`type`,`len`,`word`,`freq`) VALUES(%s,%s,%s,%s);"
		global zone
		zone = datetime.datetime.now()
		with open(file_name) as fp:
			contents = fp.readlines()
			s = main(contents)
		params = [('A', len(k), k, s[k]) for k in s]
		msql.executemany(sql_, params)
		os.remove(file_name)
		# return render_template('index.html',u=s)
		return redirect(url_for('WordList'))

	return render_template('upload.html')


import datetime


@app.route("/WordList")
def WordList():
	msql = mysql.MySQL()
	sql_ = "SELECT * FROM fenci_result;"
	u = msql.returnList(sql_)
	time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
	t = datetime.datetime.now() - zone
	return render_template('index.html', u=u, time=time, spendtime=t)


@app.route("/sortedWordList")
def sortedWordList():
	msql = mysql.MySQL()
	sql_ = "SELECT * FROM fenci_result order by freq  desc;"
	u = msql.returnList(sql_)
	time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
	t = ''
	return render_template('index.html', u=u, time=time, spendtime=t)


import csv


@app.route("/export")
def export():
	msql = mysql.MySQL()
	sql_ = "SELECT * FROM fenci_result;"
	u = msql.returnList(sql_)
	time = datetime.datetime.now().strftime("%y%m%d%H%M")
	with open(APP_STATIC_TXT+'Export\\%s.csv' % time, 'w') as f:
		writer = csv.writer(f)
		writer.writerow(['WordID', 'File/TextType', 'WordLength', 'Word', 'Freq'])
		writer.writerows(u)

	return render_template('success.html')


if __name__ == '__main__':
	app.run()
