#-*-encoding=utf-8 -*-
import time
import re
import numpy as np
import pandas as pd
from numpy import log
from itertools import chain
import pymysql
from WORD.sql import sql_db
import datetime
import threading
import os
from  WORD import sql
#tags已经处理了中文问题
def filter_list(t):
    #删除不带中文的词，匹配式
    ch=re.compile(u'.*[\u4E00-\u9FA5].*')#
    return list(filter(ch.match, t))#filter过滤器，（func，str）


#过滤Cdata
def filter_cdata(htmlstr):
    re_cdata = re.compile('//<!\[CDATA\[[^>]*//\]\]>', re.I)  # 匹配CDATA
    re_script = re.compile('<\s*script[^>]*>[^<]*<\s*/\s*script\s*>', re.I)  # Script
    re_style = re.compile('<\s*style[^>]*>[^<]*<\s*/\s*style\s*>', re.I)  # style
    re_br = re.compile('<br\s*?/?>')  # 处理换行
    re_h = re.compile('</?\w+[^>]*>')  # HTML标签
    re_comment = re.compile('<!--[^>]*-->')  # HTML注释
    re_stopwords = re.compile('\u3000')  # 去除无用的'\u3000'字符
    re_at = re.compile('@[^ ]*?') #处理微博用户名@someone

    s = re_cdata.sub(' ', htmlstr)  # 去掉CDATA
    s = re_script.sub(' ', s)  # 去掉SCRIPT
    s = re_style.sub(' ', s)  # 去掉style
    s = re_br.sub(' ', s)  # 将br转换为换行
    s = re_h.sub(' ', s)  # 去掉HTML 标签
    s = re_comment.sub(' ', s)  # 去掉HTML注释
    s = re_stopwords.sub(' ', s)

    # 去掉多余的空行
    blank_line = re.compile('\n+')
    s = blank_line.sub(' ', s)

    pat_ncen = re.compile('[^0-9a-zA-Z\u4E00-\u9FA5]')  # 匹配中英数字
    s = pat_ncen.sub(' ', s)
    #s = replaceCharEntity(s)  # 替换实体
    return s

def cal_S(sl): #信息熵计算函数
    return -((sl/sl.sum()).apply(log)*sl/sl.sum()).sum()



def ch_Clear(contents):
    s_list = []
    for content in contents:
        try:
            s_list.extend(filter_cdata(content).split())
        except Exception as err:
            print(err)
    else:
        return s_list

def openFile(filepath,mode='r',encoding = 'utf8'):
    with open(filepath,mode=mode,encoding = encoding, errors = 'ignore') as f:
        contents = f.readlines()
        s_list = []
        for content in contents:
            try:
                s_list.extend(filter_cdata(content).split())
            except Exception as err:
                print(err)
        else:
            return s_list


def main(contents='',min_count=2,min_support=20,min_s=1,max_sep=7):
    """
    min_count = 80 # 录取词语最小出现次数
    min_support = 20  # 录取词语最低支持度，1代表着随机组合
    min_s = 3 # 录取词语最低信息熵，越大说明越有可能独立成词
    max_sep = 7  # 候选词语的最大字数
    """
    myre = {2: '(..)', 3: '(...)', 4: '(....)', 5: '(.....)', 6: '(......)', 7: '(.......)'}
    s_list = ch_Clear(contents)
    #s_list = openFile(fname)
    t = []  # 保存结果用。
    if 1:
        t.append(pd.Series(list(chain(*s_list))).value_counts())  # 逐字统计
        tsum = t[0].sum()  # 统计总字数
        rt = []  # 保存结果用
        count = 1
        for m in range(2, max_sep + 1):
            #print(u'线程%d-*-正在生成%s字词...' % (li+1,m))
            t.append([])
            for i in range(m):  # 生成所有可能的m字词
                t[m - 1] = t[m - 1] + list(chain(\
                    *list(map(lambda x: re.findall(myre[m], x[i:]), s_list))))
            t[m - 1] = pd.Series(t[m - 1]).value_counts()  # 逐词统计
            t[m - 1] = t[m - 1][t[m - 1] > min_count]  # 最小次数筛选
            tt = t[m - 1][:]


            for k in range(m - 1):
                qq = np.array(list(map(lambda ms: \
                    tsum * t[m - 1][ms] / t[m - 2 - k][ms[:m - 1 - k]] / t[k][ms[m - 1 - k:]],
                    tt.index))) > min_support  # 最小支持度筛选。
                tt = tt[qq]
            rt.append(tt.index)

        for i in range(2, max_sep + 1):
            #print(u'线程%d-*-正在进行%s字词的左右邻熵筛选(%s)...' % (li+1, i, len(rt[i - 2])))
            pp = []  # 保存所有的左右邻结果
            for j in range(i):
                pp = pp + list(chain(*list(map(lambda s: re.findall('(.)%s(.)' %myre[i], s[j:]), s_list))))
            pp = pd.DataFrame(pp).set_index(1).sort_index()  # 先排序，可以加快检索速度
            index = np.sort(np.intersect1d(rt[i - 2], pp.index))  # 作交集
            # 下面两句分别是左邻和右邻信息熵筛选
            index = index[np.array(list(map(lambda s: cal_S(pd.Series(pp[0][s]).value_counts()), index))) > min_s]
            rt[i - 2] = index[np.array(list(map(lambda s: cal_S(pd.Series(pp[2][s]).value_counts()), index))) > min_s]



        for i in range(len(rt)):
            t[i + 1] = t[i + 1][filter_list(rt[i])]  # 删除无中文的词
            #print('-*-',len(t[i+1]))
                
        pt=pd.DataFrame(pd.concat(t[1:])).to_dict()[0]
        #pt = t[1:]
        return pt



