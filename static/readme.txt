└── app
    ├── configure.py
    ├── Export
    ├── __pycache__
    │   └── configure.cpython-35.pyc
    ├── static
    │   └── uploads
    ├── templates
    │   ├── base.html
    │   ├── home.html
    │   ├── index.html
    │   ├── success.html
    │   └── upload.html
    ├── web.py
    └── WORD
        ├── hongloumeng
        ├── mysql.py
        ├── part_new
        ├── __pycache__
        │   ├── fenci.cpython-35.pyc
        │   ├── mysql.cpython-35.pyc
        │   ├── sql.cpython-35.pyc
        │   └── wordCut.cpython-35.pyc
        ├── sql.py
        └── wordCut.py

