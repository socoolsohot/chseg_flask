import pymysql
import logging
sql_db={'host':'localhost',
		'user' : 'root',
    	'passwd' : '',
		'db' : 'fenci'}

fenci_result = "create table if not exists %s(\
            `id` int unsigned not null auto_increment,\
			`type` varchar(10), \
			`len` int unsigned ,\
			`word` varchar(10),\
			`freq` int(6) unsigned,\
			PRIMARY KEY(`id`))\
			ENGINE=InnoDB DEFAULT CHARSET=utf8;"%'`fenci_result`'

def initSQL():
	db = pymysql.connect(host=sql_db['host'], user=sql_db['user'], passwd=sql_db['passwd'],db=sql_db['db'])
	#print('=======use database :%s========'%sql_db['db'])
	cursor = db.cursor()
	try:
		cursor.execute('drop table if exists `fenci_result`;')
		cursor.execute(fenci_result)
		cursor.execute('create unique index in_dex on fenci_result(word);')
		print('create table fenci_result successfully!')
		db.commit()
	except Exception as err:
		print(err)
	finally:
		db.close()
		print('close mysql!')
