import logging
import pymysql

class MySQL(object):
    def __init__(self, charset='utf8'):
         self.conn =pymysql.connect(
                host = '127.0.0.1',
                port = 3306,
                user = 'root',
                passwd = 'root',
                db = 'fenci',
                charset = charset)
    def get_cursor(self):
        return self.conn.cursor()

    def query(self, sql):
        cursor = self.get_cursor()  
        try:
            cursor.execute(sql, None)
            result = cursor.fetchall()  
        except Exception as e:
            logging.error("mysql query error: %s", e)
            return None
        finally:
            cursor.close()
        return result

    def execute(self, sql, param=None):
        cursor = self.get_cursor()
        try:
            cursor.execute(sql, param)
            self.conn.commit()
            affected_row = cursor.rowcount
        except Exception as e:
            logging.error("mysql execute error: %s", e)
            return 0
        finally:
            cursor.close()
        return affected_row

    def executemany(self, sql, params=None):
        cursor = self.get_cursor()
        try:
            cursor.executemany(sql, params)
            self.conn.commit()
            affected_rows = cursor.rowcount
        except Exception as e:
            logging.error("mysql executemany error: %s", e)
            return 0
        finally:
            cursor.close()
        return affected_rows
    
    def returnList(self,sql,param=None):
        cursor = self.get_cursor()
        try:
            cursor.execute(sql, param)
            u = cursor.fetchall()
            self.conn.commit()
            affected_row = cursor.rowcount
        except Exception as e:
            logging.error("mysql execute error: %s", e)
            return 0
        finally:
            cursor.close()
            return u
        return affected_row
		


    def close(self):
        try:
            self.conn.close()
        except:
            pass

    def __del__(self):
        self.close()
