# 毕设-中文分词以及flask联合

#### 项目介绍
简单中文分词以及flask简易搭建，有许多的不足;
主要核心文件在WORD文件夹
文件目录参考readme.txt


#### 软件架构
环境pycharm， windows/ubuntu16.04


#### 安装教程

1. 运行 untitle2.py 

#### 使用说明

仅作业余练习，不许用作商业或者其他高校课程设计和毕业设计用途。
请尊重学术原则

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

#### 主要联系邮箱
techzhang123@aliyun.com



#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)